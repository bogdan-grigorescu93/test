import * as React from "react";
import MemberIcon from "../MemberIcon/MemberIcon";
import MessageContent from "./MessageContent/MessageContent";
import "./_message.scss";

export default function({ id, sentAt, user, text, color, hideMemberIcon }) {
    return (
        <div className="message">
            <div className="message__icon">
                {hideMemberIcon || 
                    <MemberIcon color={color} title={user.name} src={`/assets/${id}.jpg`} />        
                }
            </div>
            
            <div className="message__text">
                <MessageContent 
                    userName={user.name}
                    color={hideMemberIcon ? "white" : color}
                    time={sentAt}
                    text={text} />
            </div>        
        </div> 
    );
}