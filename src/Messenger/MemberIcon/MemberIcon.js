import * as React from "react";
import "./_member-icon.scss";

export default function ({ src, title, color }) {
    return (
        <img className={`member-icon member-icon--${color}`}
             src={process.env.PUBLIC_URL + src} 
             alt={title} title={title} />
    );
}
