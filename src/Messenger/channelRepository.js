
const getMessages = function(channelID) {
    return fetch(`http://new.visit-x.net/rest/v1/recruiting/messenger/channel/${channelID}`, {
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json"
        }
    })
    .then(r => r.json())
    .then(response => response.data && response.data.messages);
}

const pushMessage = function (channelID, text) {
    return fetch(`https://new.visit-x.net/rest/v1/recruiting/messenger/channel/${channelID}`, {
        method: "POST",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            text
        })
    }) 
    .then(r => r.json())
    .then(response => response.data && response.data.message);
}

export { getMessages, pushMessage };