import * as React from "react";
import "./_messenger.scss";
import * as _ from "lodash";

import Header from "./Header/Header";
import MemberIcon from "./MemberIcon/MemberIcon";
import { ReactComponent as PlusIcon } from "./plus.svg";
import Message from "./Message/Message";
import ActionBar from "./ActionBar/ActionBar";

import * as channel from "./channelRepository";

const colors = {
    "1": "blue",
    "2": "orange",
    "3": "pink"
};

export default class Messenger extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: [],

            actionBarInputValue: ""
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.messagesListRef = React.createRef();
    }

    componentDidMount() {
        channel.getMessages(1234)
            .then(messages => {
                this.setState({ messages });
            });
    }

    onSubmit() {
        if (!this.state.actionBarInputValue) {
            return;
        }

        this.setState({ actionBarInputValue: "" });
        channel.pushMessage(1234, this.state.actionBarInputValue)
            .then(message => {
                this.setState({ 
                    messages: [
                        ...this.state.messages,
                        message
                    ]
                });
            })
    }

    componentDidUpdate() {
        if (this.messagesListRef.current){
            this.messagesListRef.current.scrollTop = this.messagesListRef.current.scrollHeight;
        } 
    }

    render() {
        // I don't have an api to find out the list of members,
        // so I'll use this instead
        const members = _.uniqBy(this.state.messages.map(m => m.user), "id");

        return (
            <div className="messenger">
                <div className="messenger__frame">

                    <header className="messenger__header">
                        <Header title="Julia's Groupchat" peopleOnlineCount={3} />
                    </header>


                    <div className="messenger__members">
                        {members.map(({ id, name }) => (
                            <div key={id} className="u-margin-right-small">
                                <MemberIcon color={colors[id]} title={name} src={`/assets/${id}.jpg`} />
                            </div>
                        ))}

                        <PlusIcon className="messenger__add-member-button u-margin-top-small u-margin-left-medium" />
                    </div>


                    <div className="messenger__message-list"  ref={this.messagesListRef}>
                        <div className="messenger__message-list__wrapper">
                            {this.state.messages.map(message => (
                                <Message
                                    key={message.id} 
                                    {...message} 
                                    hideMemberIcon={message.user.name === "You"}
                                    color={colors[message.user.id] || "white"}/>
                            ))}
                        </div>
                    </div>


                    <div className="messenger__action-bar">
                        <ActionBar 
                            value={this.state.actionBarInputValue}
                            onChange={(evt) => this.setState({actionBarInputValue: evt.target.value})} 
                            onSubmit={this.onSubmit}
                            />
                    </div>

                </div>
            </div>
        );
    }
}