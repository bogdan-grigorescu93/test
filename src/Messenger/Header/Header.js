import * as React from "react";
import "./_header.scss";
import { ReactComponent as Arrow } from "./arrow.svg";

export default function ({ title, peopleOnlineCount }) {

    return (
        <div className="header">
            <div className="header__title  u-margin-bottom-small">{title}</div>
            
            <div className="header__people-counter">{peopleOnlineCount || 0} People Online</div>
            
            <Arrow className="header__back-button" alt="back button"/>
        </div>
    );

}

