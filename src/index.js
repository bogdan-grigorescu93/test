import React from 'react';
import ReactDOM from 'react-dom';
import Messenger from './Messenger/Messenger'

import "./_reset.scss";
import "./_variables.scss";
import "./_utils.scss";
                

ReactDOM.render(<Messenger />, document.getElementById('root'));

