import * as React from "react";
import { ReactComponent as SmileyIcon } from "./smiley.svg";
import "./_smiley-input.scss";

export default function ({ value, onChange, placeholder }) {
    return (
        <div className="smiley-input">
            <input
                className="smiley-input__input"
                type="text" 
                value={value}
                onChange={onChange}
                placeholder={placeholder} />

            <div className="smiley-input__icon-wrapper" >
                <SmileyIcon  className="smiley-input__icon"/>
            </div>
            
        </div>
    );
}
