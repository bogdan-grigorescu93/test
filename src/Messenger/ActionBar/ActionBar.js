import * as React from "react";

import { ReactComponent as SubmitIcon } from "./submit.svg";
import SmileyInput from "./SmileyInput/SmileyInput";
import "./_action-bar.scss";

export default class ActionBar extends React.Component {
    constructor(props) {
        super(props);

        this.onSubmit = this.onSubmit.bind(this);
    }

    render () {
        const { value, onChange } = this.props;
      
        return (
            <form className="action-bar" onSubmit={this.onSubmit}>
                <div className="action-bar__input">
                    <SmileyInput value={value} onChange={onChange} placeholder="Type something..." />
                </div>
                <button className="action-bar__btn" type="submit">
                    <SubmitIcon />
                </button>
            </form>
        );
    }

    onSubmit(evt) {
        evt.preventDefault();
        if (this.props.onSubmit) {
            this.props.onSubmit();
        }
    }
}
