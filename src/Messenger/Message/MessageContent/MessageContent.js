import * as React from "react";
import "./_message-content.scss";
import moment from "moment";

export default function ({ userName, color, sentAt, text }) {
    const time = moment(sentAt).format("h:mm A");
    
    return (
        <div className={`message-content message-content--${color}`}>
            <div className="message-content__header">
                <div className="message-content__user-name">{userName}</div>
                <div className="message-content__time">{time}</div>
            </div>
            
            <div className="message-content__text">{text}</div>
        </div>
    );
}